Jeu de Shifumi réalisé et mis en place par COLLIN Tom et PERRIER Nathan
==

<h2>Description</h2>

Ce projet a pour objectif la réalisation d'un site internet au travers de quatres exercices
distincts afin de pouvoir jouer au célèbre "Pierre, feuille, ciseaux" contre un ordinateur. Ce projet a été réalisé en 8h dans le cadre d'un Examun pour l'école Campus Academy.

<h2>Enjeux</h2>

Les enjeux sont différents selon les exercices, le but principal est au minimum d'arriver à 
finir la maquette de base qui est le jeu de shifumi avec une réponse aléatoire du "bot".

Le niveau 2 ne rajoute pas énormément de difficulté hormis celle de mettre en place des statistiques des parties jouées contre l'ordinateur.

Néanmoins la spécificité du niveau 3 est tout autre car il faut mettre en place une IA de base qui mémorise les parties précédentes et jouera en conséquence.

Le dernier niveau consiste lui à stocker dans une base de données des valeurs correspondants au taux de réussite, à l'adresse IP du joueur, ...

<h3>Marche à suivre</h3>

Pour utiliser notre site, il suffit de se rendre sur <a href="https://shifumibytomandnathzer.herokuapp.com/">ce lien</a> et de cliquer sur le seul bouton de la page, puis vous serez redirigé sur l'interface graphique du jeu et passerez plusieurs heures à vouloir gagner contre votre propre ordinateur, bonne chance !